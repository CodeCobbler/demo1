﻿$PBExportHeader$demo1.sra
$PBExportComments$Generated Application Object
forward
global type demo1 from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global type demo1 from application
string appname = "demo1"
end type
global demo1 demo1

on demo1.create
appname="demo1"
message=create message
sqlca=create transaction
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on demo1.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;//test2
end event

